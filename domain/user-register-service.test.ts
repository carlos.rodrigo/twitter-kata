import RegisterUserService from "./user-register-service"
import Repository from "./user-register-repository"
import User from "./user"

import SpyInstance = jest.SpyInstance

describe("Register user service cases", () => {
  test("Given a realname and a nickname, when try to register the user, then store the user in a repository", () => {
    const repository = new MockRepository()
    const service = new RegisterUserService(repository)
    const spyInstance = jest.spyOn(repository, 'Save')
    
    service.Register("carlos", "@carlos")

    expect(spyInstance).toHaveBeenCalled()
    expect(spyInstance).toHaveBeenCalledWith(new User("carlos", "@carlos"));
  })
  test("Given a registered nickname, when try to register the user, then the register method fail", () => {



  }

})

class MockRepository implements Repository {
  Save(u: User): void {
  }
}

"use strict";
exports.__esModule = true;
var user_register_service_1 = require("./user-register-service");
var user_1 = require("./user");
describe("Register user service cases", function () {
    test("Given a realname and a nickname, when try to register the user, then store the user in a repository", function () {
        var repository = new MockRepository();
        var service = new user_register_service_1["default"](repository);
        var spyInstance = jest.spyOn(repository, 'Save');
        service.Register("carlos", "@carlos");
        expect(spyInstance).toHaveBeenCalled();
        expect(spyInstance).toHaveBeenCalledWith(new user_1["default"]("carlos", "@carlos"));
    });
});
var MockRepository = /** @class */ (function () {
    function MockRepository() {
    }
    MockRepository.prototype.Save = function (u) {
    };
    return MockRepository;
}());

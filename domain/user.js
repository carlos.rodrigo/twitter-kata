"use strict";
exports.__esModule = true;
var User = /** @class */ (function () {
    function User(realName, nickname) {
        this.realName = realName;
        this.nickname = nickname;
    }
    return User;
}());
exports["default"] = User;

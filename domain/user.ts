

export default class User {
  private realName: string
  private nickname: string
  constructor(realName: string, nickname: string){
    this.realName = realName
    this.nickname = nickname
  }
}

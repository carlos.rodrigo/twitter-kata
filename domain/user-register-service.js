"use strict";
exports.__esModule = true;
var user_1 = require("./user");
var RegisterUserService = /** @class */ (function () {
    function RegisterUserService(repository) {
        this.repository = repository;
    }
    RegisterUserService.prototype.Register = function (realName, nickname) {
        var user = new user_1["default"](realName, nickname);
        try {
            this.repository.Save(user);
            return true;
        }
        catch (_a) {
            return false;
        }
    };
    return RegisterUserService;
}());
exports["default"] = RegisterUserService;

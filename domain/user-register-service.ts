import Repository from "./user-register-repository";
import User from "./user"

export default class RegisterUserService {
  private repository : Repository
  constructor(repository: Repository){
    this.repository = repository;
  }

  Register(realName: string, nickname: string): boolean {
    const user = new User(realName, nickname)
    try {
      this.repository.Save(user)
      return true
    }catch {
      return false
    }
  }
}

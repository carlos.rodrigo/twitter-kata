"use strict";
exports.__esModule = true;
var RegisterUserAction = /** @class */ (function () {
    function RegisterUserAction(service) {
        this.service = service;
    }
    RegisterUserAction.prototype.Register = function (realName, nickname) {
        var result = this.service.Register(realName, nickname);
        if (result) {
            return {
                ok: true,
                message: "User registered"
            };
        }
        else {
            return {
                ok: false,
                message: result
            };
        }
    };
    return RegisterUserAction;
}());
exports["default"] = RegisterUserAction;

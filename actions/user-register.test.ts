import RegisterUserAction from "./user-register"
import RegisterUserService from "../domain/user-register-service"
import Repository from "../domain/user-register-repository"



describe("Register User Action", () => {
  test("Given a real name and a nickname, when we try to register the user, then the accions is success", () => {
    let repo = newMockRepository();
    let service = new RegisterUserService(repo);
    let action = new RegisterUserAction(service);
    let result = action.Register("Carlos Rodrigo", "@carlos");
    expect(result.ok).toBe(true);
    expect(result.message).toBe("User registered");
  });
  test("Given an existing nickname, when we try to register the user, then the action will fail", () => {
    let name = "Carlos Rodrigo";
    let nickname = "@carlos";
    let repo = newMockRepository();
    let service = new RegisterUserService(repo);
    let action = new RegisterUserAction(service);

    let result = action.Register(name, nickname);
    result = action.Register("Charly", nickname);

    expect(result.ok).toBe(false);
    expect(result.message).toBe("Nickname is taken");
  })
});

function newMockRepository(): Repository {
  return new MockRepository();
}

class MockRepository implements Repository{
}




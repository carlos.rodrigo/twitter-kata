import RegisterUserService from "../domain/user-register-service";

export default class RegisterUserAction {
  private service : RegisterUserService
  constructor(service: RegisterUserService){
    this.service = service;
  }

  Register(realName: string, nickname: string){
    let result = this.service.Register(realName, nickname);
    if(result){
      return {
        ok: true,
         message: "User registered"
      };
    } else {
      return {
        ok: false,
        message: result
      };
    }
  }
}

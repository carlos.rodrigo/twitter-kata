"use strict";
exports.__esModule = true;
var user_register_1 = require("./user-register");
var user_register_service_1 = require("../domain/user-register-service");
describe("Register User Action", function () {
    test("Given a real name and a nickname, when we try to register the user, then the accions is success", function () {
        var repo = newMockRepository();
        var service = new user_register_service_1["default"](repo);
        var action = new user_register_1["default"](service);
        var result = action.Register("Carlos Rodrigo", "@carlos");
        expect(result.ok).toBe(true);
        expect(result.message).toBe("User registered");
    });
    test("Given an existing nickname, when we try to register the user, then the action will fail", function () {
        var name = "Carlos Rodrigo";
        var nickname = "@carlos";
        var repo = newMockRepository();
        var service = new user_register_service_1["default"](repo);
        var action = new user_register_1["default"](service);
        var result = action.Register(name, nickname);
        result = action.Register("Charly", nickname);
        expect(result.ok).toBe(false);
        expect(result.message).toBe("Nickname is taken");
    });
});
function newMockRepository() {
    return new MockRepository();
}
var MockRepository = /** @class */ (function () {
    function MockRepository() {
    }
    return MockRepository;
}());
